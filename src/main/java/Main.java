import logic.EntityDao;
import logic.HibernateUtil;
import logic.InventoryManager;
import logic.InvoiceManager;
import model.Inventory;
import model.Product;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import static logic.InvoiceManager.newInvoice;
import static logic.InvoiceManager.saleToInvoice;
import static logic.ProductManager.addProduct;
import static logic.ProductManager.listInventories;
import static logic.ProductManager.listProducts;
import static logic.InventoryManager.listAllInventories;

public class Main {
    public static void main(String[] args) {
        EntityDao entityDao = new EntityDao();
        Scanner scanner = new Scanner(System.in);
        boolean working = true;

        do {
            String linia = scanner.nextLine();
            if (linia.equalsIgnoreCase("quit")) {
                working = false;
            }else {
                String[] slowa = linia.split(" ");
                String komenda = slowa[0];
                if (komenda.equalsIgnoreCase("add_product")) {
                    addProduct(entityDao, slowa);
                } else if (komenda.equalsIgnoreCase("add_inventory")) {
                    InventoryManager inventoryManager = new InventoryManager();
                    inventoryManager.createNewInventoryForProduct(slowa[1], slowa[2], Long.parseLong(slowa[3]));
                } else if (komenda.equalsIgnoreCase("list_products")) {
                    List<Product> lista = listProducts();
                    lista.forEach(System.out::println);
                } else if (komenda.equalsIgnoreCase("list_inventories")) {
                    Optional<List<Inventory>> lista = listInventories(Long.parseLong(slowa[1]));
                    System.out.println(lista.toString());
                } else if (komenda.equalsIgnoreCase("list_full_inventory")) {
                    List<Inventory> lista = listAllInventories();
                    lista.forEach(System.out::println);
                } else if (komenda.equalsIgnoreCase("add_invoice")){
                    newInvoice(entityDao, slowa);
                } else if (komenda.equalsIgnoreCase("add_sale")){
                    saleToInvoice(Long.parseLong(slowa[1]),slowa[2],slowa[3]);
                }
            }

        } while (working);

        HibernateUtil.getSessionFactory().close();
    }
}
