package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity

public class Invoice implements IBaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
   private long id;
    @CreationTimestamp
    private LocalDate datecreated;

    @OneToMany(mappedBy = "invoiceid", fetch= FetchType.EAGER) //
    private List<ProductSale> sales;

    public Invoice (long id){this.id=id;}
}
