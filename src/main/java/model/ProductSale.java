package model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity

public class ProductSale implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    private String quantity;
    private String price;

    long invoiceid;
}
