package logic;

import model.Inventory;
import model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class InventoryManager {
    public void createNewInventoryForProduct(String quantity, String value,Long productId){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try{
            transaction = session.beginTransaction();

            Inventory inventory = new Inventory();
            inventory.setQuantity(quantity);
            inventory.setValue(value);
            Product product = session.get(Product.class, productId);


            inventory.setProduct(product);
            product.getInventories().add(inventory);

            session.update(product);
//            session.update(orderRepair);
            session.save(inventory);
            transaction.commit();
        } catch(Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        } if(session !=null && session.isOpen()){
            session.close();
        }
    }
    public static List<Inventory> listAllInventories(){
        List<Inventory> items = new ArrayList<>();
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<Inventory> query = builder.createQuery(Inventory.class);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<Inventory> tableRoot = query.from(Inventory.class);

            // wykonuję zapytanie(query) w tabeli (tableRoot)
            query.select(tableRoot);

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }
        return items;
    }
}
