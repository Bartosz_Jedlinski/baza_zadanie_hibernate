package logic;

import model.Inventory;
import model.Invoice;
import model.Product;
import model.ProductSale;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class InvoiceManager {

        public static void newInvoice(EntityDao entityDao, String[]slowa){
            Invoice invoice = new Invoice();
            entityDao.save(invoice);
            System.out.println("Stworzono nową fakturę o identyfikatorze: " + invoice.getId());
        }
        public static void saleToInvoice (Long invoiceId, String quantity, String price){
            SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
            Transaction transaction = null;
            try (Session session = sessionFactory.openSession()) {
                transaction = session.beginTransaction();
                ProductSale productSale = new ProductSale();
                productSale.setQuantity(quantity);
                productSale.setPrice(price);
                Invoice invoice = session.get(Invoice.class, invoiceId);

                    if (invoiceId != null){
                        productSale.setInvoiceid(invoice.getId());
                        invoice.getSales().add(productSale);

                    session.update(invoice);
//            session.update(orderRepair);
                    session.save(productSale);
                        transaction.commit();
                        System.out.println("Dodano sprzedaż do faktury o identyfkatorze: " + invoiceId);

                }
            } catch (Exception sqle) {
                System.err.println("Error: " + sqle.getMessage());
            }

            }
        }


