package logic;

import model.IBaseEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EntityDao {
    public void save(IBaseEntity objectToSave){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;
        try(Session session = sessionFactory.openSession()){ //dzięki try with resorces nie musimy robić close na sesji
            transaction = session.beginTransaction();

            session.save(objectToSave);

            transaction.commit();
        } catch(Exception e){
            if(transaction!=null){
                transaction.rollback();
            }
        }

    }
    public <T extends IBaseEntity> List<T> getListOfAll(Class<T> classType) {
        List<T> list = new ArrayList<>();

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<T> query = builder.createQuery(classType);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<T> tableRoot = query.from(classType);

            // wykonuję zapytanie(query) w tabeli (tableRoot)
            query.select(tableRoot);

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }

        return list;
    }
    public <T extends IBaseEntity> Optional<T> getById(Class<T> classType, Long searchedId){
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<T> query = builder.createQuery(classType);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<T> tableRoot = query.from(classType);

            // wykonuję zapytanie(query) w tabeli (tableRoot)
            query.select(tableRoot).where(builder.equal(tableRoot.get("product_id"), searchedId));

            return Optional.of(session.createQuery(query).getSingleResult());
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }

        return Optional.empty();
    }

//    Alternatywa
//    public <T extends IBaseEntity> Optional<T> getById(Class<T> classType, Long searchedId){
//        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
//        try (Session session = sessionFactory.openSession()) {
//
//            return Optional.of(session.get(classType, searchedId));
//        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
//            System.err.println("Error: " + sqle.getMessage());
//        }
//
//        return Optional.empty();
}
