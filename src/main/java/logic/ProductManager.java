package logic;

import model.Inventory;
import model.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductManager {
    public static void addProduct(EntityDao entityDao, String[]slowa){
        Product product = new Product(slowa[1]);
        entityDao.save(product);
        System.out.println("Dodano nowy produkt: " + product);
    }
    public static List<Product> listProducts(){
        List<Product> products = new ArrayList<>();
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
            CriteriaQuery<Product> query = builder.createQuery(Product.class);

            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<Product> tableRoot = query.from(Product.class);

            // wykonuję zapytanie(query) w tabeli (tableRoot)
            query.select(tableRoot);

            return session.createQuery(query).getResultList();
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }
        return products;
    }
    public static Optional <List<Inventory>> listInventories(Long productId){
//        List<Inventory> items = new ArrayList<>(); // to nie jest konieczne ponieważ wszystko wrzucam do optionala
                                                    //  i nie zwracam items
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();

            // zapytanie na podstawie criteria buildera o CriteriaQuery
           CriteriaQuery<Inventory> query = builder.createQuery(Inventory.class);


            // z obiektu root możemy pobrać wartości kolumn.
            // tworzymy go żeby mówić w jakiej tabeli szukamy
            Root<Inventory> tableRoot = query.from(Inventory.class);


            // wykonuję zapytanie(query) w tabeli (tableRoot)
            query.select(tableRoot).where(builder.equal(tableRoot.get("product_id"), productId));

            return Optional.of(session.createQuery(query).getResultList());
        } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
            System.err.println("Error: " + sqle.getMessage());
        }
        return Optional.empty();

    }
}
